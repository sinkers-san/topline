import Vue from 'vue'
import Router from 'vue-router'
import fractionsWrapper from '@/components/fractionsWrapper'
import commentsWrapper from '@/components/commentsWrapper'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/",
      name: "Fractions",
      component: fractionsWrapper
    },
    {
      path: "/comments",
      name: "Comments",
      component: commentsWrapper
    }
  ]
})
